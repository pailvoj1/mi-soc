/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
#include "xil_cache.h"
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "ps7_init.h"
#include "xil_io.h"
#include "xparameters.h"
//interrupts
#include "xscugic.h"

//DMA addresses for data transfers
#define DMA_S2MM_DA 0xa000000
#define DMA_MM2S_SA 0xa000008

//interrupt related variables
XScuGic InterruptController;
static XScuGic_Config *GicConfig;
volatile int s2mm_int = 0;
volatile int mm2s_int = 0;

//static u16 H[4] = {0x00F8, 0x0714, 0x0B62, 0x0DA1};

//G matrix for Hamming encode
static u16 G[8] = {0x0803, 0x0405, 0x0206,0x0107 ,0x0089, 0x004A, 0x002B, 0x001C};

//Hamming code related widths
static int outBits = 12;
static int  inBits =  8;

//Encode one word by Hamming
u32 hammingEncode (u32 uncodedNum) {

  int i,j;

  u32 codedNum = 0;
  u16 inNum = (u16)uncodedNum & 0x00FF;   //only 8 bits - 8 word
  unsigned int codedBit = 0;

  //shift through all 12 output bits
  for (i = 0; i < outBits; i++) {
    //reset temp Bit for coded word
    codedBit = 0;
    //shift resulting word to be ready for new bit
    codedNum = codedNum << 1;
    //shift through every line in G matrix
    for (j = 0; j < inBits; j++) {
      //get the correct bit from input vector
      unsigned int numBit = ((inNum << j) & 0x0080) >> 7;
      //get the correct bit from matrix G
      unsigned int matrixBit = ((G[j] << i) & 0x0800) >> 11;
      //and bits from number and matrix and XOR its result to temp result
      codedBit ^= numBit & matrixBit;
    }
    //after all ands and xors from one bit of inNumber, xor it into the result
    codedNum =codedNum ^ codedBit;
  }

  return codedNum;
}

//decode one word by Hamming
u32 hammingDecode (u32 codedNum) {

  //clear from not used bits
  u32 inNum = codedNum & 0x00000FFF;   //only 12 bits - 8 word, 4 parity

  //return only valid bits(11 downto 4) without parity bits
  return inNum >> 4;

}

//Start DMA transfer PS -> PL
void StartDMATransferMM2S(unsigned int srcAddress, unsigned int len) {
	//write source address to MM2S_SA reg
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x18, srcAddress);

	//write length to MM2S_LENGTH reg
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x28, len);
}

//Start DMA transfer PL -> PS
void StartDMATransferS2MM(unsigned int dstAddress, unsigned int len) {
	//write destination address to S2MM_DA reg
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x48, dstAddress);

	//write length to S2MM_LENGTH reg
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x58, len);
}

//PL -> PS transfer done interrupt handler
void InterruptHandlerS2MM() {
	//first you should turn off interrupts, but DMA doesn't produce them so fast
	u32 tmp;

	//clear interrupt
	tmp = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + 0x34);
	tmp |= 0x1000;
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x34, tmp);

	//process data
	s2mm_int = 1;
}

//PS -> PL transfer done interrupt handler
void InterruptHandlerMM2S() {
	//first you should turn of interrupts, but DMA doesn't produce them so fast
	u32 tmp;

	//clear interrupt
	tmp = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + 0x04);
	tmp |= 0x1000;
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x04, tmp);

	//process data
	mm2s_int = 1;
}


int setup_interrupt_system(XScuGic *XScuGicInstancePtr) {
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler) XScuGic_InterruptHandler, XScuGicInstancePtr );
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}
//init interrupt system
int initialize_interrupt_system(u16 deviceID) {
	int Status;

	GicConfig = XScuGic_LookupConfig(deviceID);
	if (GicConfig == NULL) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(&InterruptController,GicConfig, GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = setup_interrupt_system(&InterruptController);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = XScuGic_Connect(&InterruptController, XPAR_FABRIC_AXI_DMA_0_S2MM_INTROUT_INTR, (Xil_ExceptionHandler) InterruptHandlerS2MM, NULL);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = XScuGic_Connect(&InterruptController, XPAR_FABRIC_AXI_DMA_0_MM2S_INTROUT_INTR, (Xil_ExceptionHandler) InterruptHandlerMM2S, NULL);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XScuGic_Enable(&InterruptController, XPAR_FABRIC_AXI_DMA_0_S2MM_INTROUT_INTR);
	XScuGic_Enable(&InterruptController, XPAR_FABRIC_AXI_DMA_0_MM2S_INTROUT_INTR);

	return XST_SUCCESS;
}

//init DMA
int initialize_axi_dma() {
	u32 tmp;

	//S2MM En + IRQ
	tmp = Xil_In32(XPAR_AXIDMA_0_BASEADDR + 0x30);
	tmp |= 0x1001;
	Xil_Out32(XPAR_AXIDMA_0_BASEADDR + 0x30, tmp);

	tmp = Xil_In32(XPAR_AXIDMA_0_BASEADDR + 0x30);
	xil_printf("DMA S2MMCR is set: %X.\n\r", tmp);

	//MM2S EN + IRQ
	tmp = Xil_In32(XPAR_AXIDMA_0_BASEADDR);
	tmp |= 0x1001;
	Xil_Out32(XPAR_AXIDMA_0_BASEADDR, tmp);

	tmp = Xil_In32(XPAR_AXIDMA_0_MICRO_DMA);
	xil_printf("DMA MM2SCR is set: %X.\n\r");
	return 0;
}
int main()
{
    init_platform();
    //enable PL
    xil_printf("Enabling PL side.\n\r");
    ps7_post_config();

    //initialize DMA
    xil_printf("Initializing DMA.\n\r");
    initialize_axi_dma();

    //initialize interrupts
    xil_printf("Initializing interrupt system.\n\r");
    initialize_interrupt_system(XPAR_PS7_SCUGIC_0_DEVICE_ID);

    //disable DCache
    Xil_DCacheDisable();

    u32 tmp;

    //Start the first DMA transfer (PL -> PS)
    xil_printf("Initializing the first DMA transfer.\n\r");
    StartDMATransferS2MM(DMA_S2MM_DA, 4);

    u32 *recData = DMA_S2MM_DA;
    u32 *sendData = DMA_MM2S_SA;


    while(1) {
    	//Transfer PL -> PS done
    	if (s2mm_int) {
    		//clear interrupt flag
    		s2mm_int = 0;
    		//get and decode data
    		tmp = hammingDecode(*recData);
    		//invert data
    		tmp = ~tmp;
    		//encode and send data
    		*sendData = hammingEncode(tmp);
    		//insert error if desired
    		*sendData |= 0x00000000;
    		StartDMATransferMM2S(DMA_MM2S_SA, 4);
    	}
    	//transfer PS -> PL done
    	if (mm2s_int) {
    		//clear interrupt flag
    		mm2s_int = 0;
    		//get new data from PL
    		StartDMATransferS2MM(DMA_S2MM_DA, 4);
    	}
    }

    cleanup_platform();
    return 0;
}
