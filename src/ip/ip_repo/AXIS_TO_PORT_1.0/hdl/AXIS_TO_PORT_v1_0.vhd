LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY AXIS_TO_PORT_v1_0 IS
	GENERIC (
		-- Users to add parameters here
        G_PORT_WIDTH   : integer := 32;
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S_AXIS
		C_S_AXIS_TDATA_WIDTH	: integer	:= 32
	);
	PORT (
		-- Users to add ports here
        data_out  :  OUT std_logic_vector(G_PORT_WIDTH - 1 DOWNTO 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S_AXIS
		s_axis_aclk	: in std_logic;
		s_axis_aresetn	: in std_logic;
		s_axis_tready	: out std_logic;
		s_axis_tdata	: in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
		s_axis_tstrb	: in std_logic_vector((C_S_AXIS_TDATA_WIDTH/8)-1 downto 0);
		s_axis_tlast	: in std_logic;
		s_axis_tvalid	: in std_logic
	);
END ENTITY AXIS_TO_PORT_v1_0;




ARCHITECTURE arch_imp OF AXIS_TO_PORT_v1_0 IS

SIGNAL capture_reg : std_logic_vector(G_PORT_WIDTH - 1 DOWNTO 0);

BEGIN


	-- Add user logic here
  capture_proc : PROCESS(s_axis_aclk)
  BEGIN
    IF (rising_edge(s_axis_aclk)) THEN
      IF (s_axis_aresetn = '0') THEN
        capture_reg <= (OTHERS => '0');
      ELSE
        IF (s_axis_tvalid = '1') THEN
          capture_reg <= (s_axis_tdata(G_PORT_WIDTH - 1 DOWNTO 0));
        END IF;
      END IF;
    END IF;
  END PROCESS capture_proc;
  
  data_out <= capture_reg;
  s_axis_tready <= '1';
	-- User logic ends

END ARCHITECTURE arch_imp;
