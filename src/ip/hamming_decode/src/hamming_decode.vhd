LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following LIBRARY declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE IEEE.NUMERIC_STD.ALL;

-- Uncomment the following LIBRARY declaration if instantiating
-- any Xilinx leaf cells in thIS code.
--LIBRARY UNISIM;
--USE UNISIM.VComponents.all;

ENTITY hamming_decode IS
  PORT (
    b    :  IN  std_logic_vector(11 DOWNTO 0);
    c    :  OUT std_logic_vector(7 DOWNTO 0)
  );
END ENTITY hamming_decode;

ARCHITECTURE rtl OF hamming_decode IS

  SIGNAL corr : std_logic_vector(7 DOWNTO 0);
  SIGNAL s    : std_logic_vector(3 DOWNTO 0);

BEGIN

  c <= b(11 DOWNTO 4) XOR corr;
  
  s(3) <= b(7) XOR b(6) XOR b(5) XOR b(4) XOR b(3);
  s(2) <= b(10) XOR b(9) XOR b(8) XOR b(4) XOR b(2);
  s(1) <= b(11) XOR b(9) XOR b(8) XOR b(6) XOR b(5) XOR b(1);
  s(0) <= b(11) XOR b(10) XOR b(8) XOR b(7) XOR b(5) XOR b(0);
  
  dec_proc : PROCESS(s)
  BEGIN
    corr <= (OTHERS => '0');
    CASE (s) IS
      WHEN "0011" =>
        corr(7) <= '1';        
      WHEN "0101" =>
        corr(6) <= '1';
      WHEN "0110" =>
        corr(5) <= '1';
      WHEN "0111" =>
        corr(4) <= '1';
      WHEN "1001" =>
        corr(3) <= '1';
      WHEN "1010" =>
        corr(2) <= '1';
      WHEN "1011" =>
        corr(1) <= '1';
      WHEN "1100" =>
        corr(0) <= '1';
      WHEN OTHERS =>
        NULL;
    END CASE;
    
  END PROCESS dec_proc;


END ARCHITECTURE rtl;
