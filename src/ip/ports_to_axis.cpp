#include "ap_int.h"

int ports_to_axis(bool en, ap_uint<12> ports, int * m_axis) {
	static int tmp;

	if (en) {
		tmp = ports;
		*m_axis = tmp;
	}

	return 0;
}
