LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following LIBRARY declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE IEEE.NUMERIC_STD.ALL;

-- Uncomment the following LIBRARY declaration if instantiating
-- any Xilinx leaf cells in thIS code.
--LIBRARY UNISIM;
--USE UNISIM.VComponents.all;

ENTITY hamming_encode IS
  PORT (
    a  :  IN  std_logic_vector(7 DOWNTO 0);
    b  :  OUT std_logic_vector(11 DOWNTO 0)
  );
END ENTITY hamming_encode;

ARCHITECTURE rtl OF hamming_encode IS

BEGIN

  b (11 DOWNTO 4) <= a;
  b(3) <= a(3) XOR a(2) XOR a(1) XOR a(0);
  b(2) <= a(6) XOR a(5) XOR a(4) XOR a(0);
  b(1) <= a(7) XOR a(5) XOR a(4) XOR a(2) XOR a(1);
  b(0) <= a(7) XOR a(6) XOR a(4) XOR a(3) XOR a(1);


END ARCHITECTURE rtl;
