#include "ap_int.h"

int ports_to_axis(bool en, ap_uint<12> ports, int * m_axis);

int main() {
	int y;

	for (ap_uint<12> i = 0; i < 10; i++) {
		ports_to_axis(false, i, &y);
	}

	for (ap_uint<12> i = 0; i < 10; i++) {
		ports_to_axis(true, i, &y);
	}

	for (ap_uint<12> i = 0; i < 10; i++) {
		ports_to_axis(false, i, &y);
	}

	return 0;
}
