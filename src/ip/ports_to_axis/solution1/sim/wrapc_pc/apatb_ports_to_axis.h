// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================

extern int AESL_WRAP_ports_to_axis (
bool en,
ap_uint<12> ports,
int* M_AXIS);
