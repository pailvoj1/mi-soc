// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================

#define AP_INT_MAX_W 32678

#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;


// [dump_struct_tree [build_nameSpaceTree] dumpedStructList] ---------->


// [dump_enumeration [get_enumeration_list]] ---------->


// wrapc file define: "en"
#define AUTOTB_TVIN_en  "../tv/cdatafile/c.ports_to_axis.autotvin_en.dat"
// wrapc file define: "ports_V"
#define AUTOTB_TVIN_ports_V  "../tv/cdatafile/c.ports_to_axis.autotvin_ports_V.dat"
// wrapc file define: "M_AXIS"
#define AUTOTB_TVOUT_M_AXIS  "../tv/cdatafile/c.ports_to_axis.autotvout_M_AXIS.dat"
#define AUTOTB_TVIN_M_AXIS  "../tv/cdatafile/c.ports_to_axis.autotvin_M_AXIS.dat"
// wrapc file define: "ap_return"
#define AUTOTB_TVOUT_ap_return  "../tv/cdatafile/c.ports_to_axis.autotvout_ap_return.dat"

#define INTER_TCL  "../tv/cdatafile/ref.tcl"

// tvout file define: "M_AXIS"
#define AUTOTB_TVOUT_PC_M_AXIS  "../tv/rtldatafile/rtl.ports_to_axis.autotvout_M_AXIS.dat"
// tvout file define: "ap_return"
#define AUTOTB_TVOUT_PC_ap_return  "../tv/rtldatafile/rtl.ports_to_axis.autotvout_ap_return.dat"

class INTER_TCL_FILE {
	public:
		INTER_TCL_FILE(const char* name) {
			mName = name;
			en_depth = 0;
			ports_V_depth = 0;
			M_AXIS_depth = 0;
			ap_return_depth = 0;
			trans_num =0;
		}

		~INTER_TCL_FILE() {
			mFile.open(mName);
			if (!mFile.good()) {
				cout << "Failed to open file ref.tcl" << endl;
				exit (1);
			}
			string total_list = get_depth_list();
			mFile << "set depth_list {\n";
			mFile << total_list;
			mFile << "}\n";
			mFile << "set trans_num "<<trans_num<<endl;
			mFile.close();
		}

		string get_depth_list () {
			stringstream total_list;
			total_list << "{en " << en_depth << "}\n";
			total_list << "{ports_V " << ports_V_depth << "}\n";
			total_list << "{M_AXIS " << M_AXIS_depth << "}\n";
			total_list << "{ap_return " << ap_return_depth << "}\n";
			return total_list.str();
		}

		void set_num (int num , int* class_num) {
			(*class_num) = (*class_num) > num ? (*class_num) : num;
		}
	public:
		int en_depth;
		int ports_V_depth;
		int M_AXIS_depth;
		int ap_return_depth;
		int trans_num;

	private:
		ofstream mFile;
		const char* mName;
};

extern int ports_to_axis (
bool en,
ap_uint<12> ports,
int* M_AXIS);

int AESL_WRAP_ports_to_axis (
bool en,
ap_uint<12> ports,
int* M_AXIS)
{
	refine_signal_handler();
	fstream wrapc_switch_file_token;
	wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
	int AESL_i;
	if (wrapc_switch_file_token.good())
	{
		CodeState = ENTER_WRAPC_PC;
		static unsigned AESL_transaction_pc = 0;
		string AESL_token;
		string AESL_num;
		static AESL_FILE_HANDLER aesl_fh;

		int AESL_return;

		// output port post check: "M_AXIS"
		aesl_fh.read(AUTOTB_TVOUT_PC_M_AXIS, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_M_AXIS, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_M_AXIS, AESL_token); // data

			sc_bv<32> *M_AXIS_pc_buffer = new sc_bv<32>[1];
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'M_AXIS', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'M_AXIS', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					M_AXIS_pc_buffer[i] = AESL_token.c_str();
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_M_AXIS, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_M_AXIS))
				{
					exit(1);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: M_AXIS
				{
					// bitslice(31, 0)
					// {
						// celement: M_AXIS(31, 0)
						// {
							sc_lv<32>* M_AXIS_lv0_0_0_1 = new sc_lv<32>[1];
						// }
					// }

					// bitslice(31, 0)
					{
						int hls_map_index = 0;
						// celement: M_AXIS(31, 0)
						{
							// carray: (0) => (0) @ (1)
							for (int i_0 = 0; i_0 <= 0; i_0 += 1)
							{
								if (&(M_AXIS[0]) != NULL) // check the null address if the c port is array or others
								{
									M_AXIS_lv0_0_0_1[hls_map_index].range(31, 0) = sc_bv<32>(M_AXIS_pc_buffer[hls_map_index].range(31, 0));
									hls_map_index++;
								}
							}
						}
					}

					// bitslice(31, 0)
					{
						int hls_map_index = 0;
						// celement: M_AXIS(31, 0)
						{
							// carray: (0) => (0) @ (1)
							for (int i_0 = 0; i_0 <= 0; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : M_AXIS[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : M_AXIS[0]
								// output_left_conversion : M_AXIS[i_0]
								// output_type_conversion : (M_AXIS_lv0_0_0_1[hls_map_index]).to_uint64()
								if (&(M_AXIS[0]) != NULL) // check the null address if the c port is array or others
								{
									M_AXIS[i_0] = (M_AXIS_lv0_0_0_1[hls_map_index]).to_uint64();
									hls_map_index++;
								}
							}
						}
					}
				}
			}

			// release memory allocation
			delete [] M_AXIS_pc_buffer;
		}

		// output port post check: "ap_return"
		aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_token); // data

			sc_bv<32> ap_return_pc_buffer;
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'ap_return', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'ap_return', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					ap_return_pc_buffer = AESL_token.c_str();
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_ap_return))
				{
					exit(1);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: ap_return
				{
					// bitslice(31, 0)
					// {
						// celement: return(31, 0)
						// {
							sc_lv<32> return_lv0_0_1_0;
						// }
					// }

					// bitslice(31, 0)
					{
						// celement: return(31, 0)
						{
							// carray: (0) => (1) @ (0)
							{
								if (&(AESL_return) != NULL) // check the null address if the c port is array or others
								{
									return_lv0_0_1_0.range(31, 0) = sc_bv<32>(ap_return_pc_buffer.range(31, 0));
								}
							}
						}
					}

					// bitslice(31, 0)
					{
						// celement: return(31, 0)
						{
							// carray: (0) => (1) @ (0)
							{
								// sub                    : 
								// ori_name               : AESL_return
								// sub_1st_elem           : 
								// ori_name_1st_elem      : AESL_return
								// output_left_conversion : AESL_return
								// output_type_conversion : (return_lv0_0_1_0).to_uint64()
								if (&(AESL_return) != NULL) // check the null address if the c port is array or others
								{
									AESL_return = (return_lv0_0_1_0).to_uint64();
								}
							}
						}
					}
				}
			}
		}

		AESL_transaction_pc++;

		return AESL_return;
	}
	else
	{
		CodeState = ENTER_WRAPC;
		static unsigned AESL_transaction;

		static AESL_FILE_HANDLER aesl_fh;

		// "en"
		char* tvin_en = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_en);

		// "ports_V"
		char* tvin_ports_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_ports_V);

		// "M_AXIS"
		char* tvin_M_AXIS = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_M_AXIS);
		char* tvout_M_AXIS = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_M_AXIS);

		// "ap_return"
		char* tvout_ap_return = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_ap_return);

		CodeState = DUMP_INPUTS;
		static INTER_TCL_FILE tcl_file(INTER_TCL);
		int leading_zero;

		// [[transaction]]
		sprintf(tvin_en, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_en, tvin_en);

		sc_bv<1> en_tvin_wrapc_buffer;

		// RTL Name: en
		{
			// bitslice(0, 0)
			{
				// celement: en(0, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : en
						// sub_1st_elem          : 
						// ori_name_1st_elem     : en
						// regulate_c_name       : en
						// input_type_conversion : en
						if (&(en) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<1> en_tmp_mem;
							en_tmp_mem = en;
							en_tvin_wrapc_buffer.range(0, 0) = en_tmp_mem.range(0, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_en, "%s\n", (en_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_en, tvin_en);
		}

		tcl_file.set_num(1, &tcl_file.en_depth);
		sprintf(tvin_en, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_en, tvin_en);

		// [[transaction]]
		sprintf(tvin_ports_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_ports_V, tvin_ports_V);

		sc_bv<12> ports_V_tvin_wrapc_buffer;

		// RTL Name: ports_V
		{
			// bitslice(11, 0)
			{
				// celement: ports.V(11, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : ports
						// sub_1st_elem          : 
						// ori_name_1st_elem     : ports
						// regulate_c_name       : ports_V
						// input_type_conversion : (ports).to_string(2).c_str()
						if (&(ports) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<12> ports_V_tmp_mem;
							ports_V_tmp_mem = (ports).to_string(2).c_str();
							ports_V_tvin_wrapc_buffer.range(11, 0) = ports_V_tmp_mem.range(11, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_ports_V, "%s\n", (ports_V_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_ports_V, tvin_ports_V);
		}

		tcl_file.set_num(1, &tcl_file.ports_V_depth);
		sprintf(tvin_ports_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_ports_V, tvin_ports_V);

		// [[transaction]]
		sprintf(tvin_M_AXIS, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_M_AXIS, tvin_M_AXIS);

		sc_bv<32>* M_AXIS_tvin_wrapc_buffer = new sc_bv<32>[1];

		// RTL Name: M_AXIS
		{
			// bitslice(31, 0)
			{
				int hls_map_index = 0;
				// celement: M_AXIS(31, 0)
				{
					// carray: (0) => (0) @ (1)
					for (int i_0 = 0; i_0 <= 0; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : M_AXIS[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : M_AXIS[0]
						// regulate_c_name       : M_AXIS
						// input_type_conversion : M_AXIS[i_0]
						if (&(M_AXIS[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> M_AXIS_tmp_mem;
							M_AXIS_tmp_mem = M_AXIS[i_0];
							M_AXIS_tvin_wrapc_buffer[hls_map_index].range(31, 0) = M_AXIS_tmp_mem.range(31, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_M_AXIS, "%s\n", (M_AXIS_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_M_AXIS, tvin_M_AXIS);
		}

		tcl_file.set_num(1, &tcl_file.M_AXIS_depth);
		sprintf(tvin_M_AXIS, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_M_AXIS, tvin_M_AXIS);

		// release memory allocation
		delete [] M_AXIS_tvin_wrapc_buffer;

// [call_c_dut] ---------->

		CodeState = CALL_C_DUT;
		int AESL_return = ports_to_axis(en, ports, M_AXIS);

		CodeState = DUMP_OUTPUTS;

		// [[transaction]]
		sprintf(tvout_M_AXIS, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_M_AXIS, tvout_M_AXIS);

		sc_bv<32>* M_AXIS_tvout_wrapc_buffer = new sc_bv<32>[1];

		// RTL Name: M_AXIS
		{
			// bitslice(31, 0)
			{
				int hls_map_index = 0;
				// celement: M_AXIS(31, 0)
				{
					// carray: (0) => (0) @ (1)
					for (int i_0 = 0; i_0 <= 0; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : M_AXIS[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : M_AXIS[0]
						// regulate_c_name       : M_AXIS
						// input_type_conversion : M_AXIS[i_0]
						if (&(M_AXIS[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> M_AXIS_tmp_mem;
							M_AXIS_tmp_mem = M_AXIS[i_0];
							M_AXIS_tvout_wrapc_buffer[hls_map_index].range(31, 0) = M_AXIS_tmp_mem.range(31, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvout_M_AXIS, "%s\n", (M_AXIS_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_M_AXIS, tvout_M_AXIS);
		}

		tcl_file.set_num(1, &tcl_file.M_AXIS_depth);
		sprintf(tvout_M_AXIS, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_M_AXIS, tvout_M_AXIS);

		// release memory allocation
		delete [] M_AXIS_tvout_wrapc_buffer;

		// [[transaction]]
		sprintf(tvout_ap_return, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_ap_return, tvout_ap_return);

		sc_bv<32> ap_return_tvout_wrapc_buffer;

		// RTL Name: ap_return
		{
			// bitslice(31, 0)
			{
				// celement: return(31, 0)
				{
					// carray: (0) => (1) @ (0)
					{
						// sub                   : 
						// ori_name              : AESL_return
						// sub_1st_elem          : 
						// ori_name_1st_elem     : AESL_return
						// regulate_c_name       : return
						// input_type_conversion : AESL_return
						if (&(AESL_return) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> return_tmp_mem;
							return_tmp_mem = AESL_return;
							ap_return_tvout_wrapc_buffer.range(31, 0) = return_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvout_ap_return, "%s\n", (ap_return_tvout_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_ap_return, tvout_ap_return);
		}

		tcl_file.set_num(1, &tcl_file.ap_return_depth);
		sprintf(tvout_ap_return, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_ap_return, tvout_ap_return);

		CodeState = DELETE_CHAR_BUFFERS;
		// release memory allocation: "en"
		delete [] tvin_en;
		// release memory allocation: "ports_V"
		delete [] tvin_ports_V;
		// release memory allocation: "M_AXIS"
		delete [] tvout_M_AXIS;
		delete [] tvin_M_AXIS;
		// release memory allocation: "ap_return"
		delete [] tvout_ap_return;

		AESL_transaction++;

		tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);

		return AESL_return;
	}
}

