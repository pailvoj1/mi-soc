
log_wave -r /
set designtopgroup [add_wave_group "Design Top Signals"]
set coutputgroup [add_wave_group "C Outputs" -into $designtopgroup]
set return_group [add_wave_group return(wire) -into $coutputgroup]
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ap_return -into $return_group -radix hex
set M_AXIS_group [add_wave_group M_AXIS(axis) -into $coutputgroup]
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/M_AXIS_TREADY -into $M_AXIS_group -color #ffff00 -radix hex
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/M_AXIS_TVALID -into $M_AXIS_group -color #ffff00 -radix hex
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/M_AXIS_TDATA -into $M_AXIS_group -radix hex
set cinputgroup [add_wave_group "C Inputs" -into $designtopgroup]
set ports_group [add_wave_group ports(wire) -into $cinputgroup]
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ports_V -into $ports_group -radix hex
set en_group [add_wave_group en(wire) -into $cinputgroup]
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/en -into $en_group -radix hex
set blocksiggroup [add_wave_group "Block-level IO Handshake" -into $designtopgroup]
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ap_start -into $blocksiggroup
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ap_done -into $blocksiggroup
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ap_idle -into $blocksiggroup
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ap_ready -into $blocksiggroup
set resetgroup [add_wave_group "Reset" -into $designtopgroup]
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ap_rst_n -into $resetgroup
set clockgroup [add_wave_group "Clock" -into $designtopgroup]
add_wave /apatb_ports_to_axis_top/AESL_inst_ports_to_axis/ap_clk -into $clockgroup
set testbenchgroup [add_wave_group "Test Bench Signals"]
set tbinternalsiggroup [add_wave_group "Internal Signals" -into $testbenchgroup]
set tb_simstatus_group [add_wave_group "Simulation Status" -into $tbinternalsiggroup]
set tb_portdepth_group [add_wave_group "Port Depth" -into $tbinternalsiggroup]
add_wave /apatb_ports_to_axis_top/AUTOTB_TRANSACTION_NUM -into $tb_simstatus_group -radix hex
add_wave /apatb_ports_to_axis_top/ready_cnt -into $tb_simstatus_group -radix hex
add_wave /apatb_ports_to_axis_top/done_cnt -into $tb_simstatus_group -radix hex
add_wave /apatb_ports_to_axis_top/LENGTH_en -into $tb_portdepth_group -radix hex
add_wave /apatb_ports_to_axis_top/LENGTH_ports_V -into $tb_portdepth_group -radix hex
add_wave /apatb_ports_to_axis_top/LENGTH_M_AXIS -into $tb_portdepth_group -radix hex
add_wave /apatb_ports_to_axis_top/LENGTH_ap_return -into $tb_portdepth_group -radix hex
set tbcoutputgroup [add_wave_group "C Outputs" -into $testbenchgroup]
set tb_return_group [add_wave_group return(wire) -into $tbcoutputgroup]
add_wave /apatb_ports_to_axis_top/ap_return -into $tb_return_group -radix hex
set tb_M_AXIS_group [add_wave_group M_AXIS(axis) -into $tbcoutputgroup]
add_wave /apatb_ports_to_axis_top/M_AXIS_TREADY -into $tb_M_AXIS_group -color #ffff00 -radix hex
add_wave /apatb_ports_to_axis_top/M_AXIS_TVALID -into $tb_M_AXIS_group -color #ffff00 -radix hex
add_wave /apatb_ports_to_axis_top/M_AXIS_TDATA -into $tb_M_AXIS_group -radix hex
set tbcinputgroup [add_wave_group "C Inputs" -into $testbenchgroup]
set tb_ports_group [add_wave_group ports(wire) -into $tbcinputgroup]
add_wave /apatb_ports_to_axis_top/ports_V -into $tb_ports_group -radix hex
set tb_en_group [add_wave_group en(wire) -into $tbcinputgroup]
add_wave /apatb_ports_to_axis_top/en -into $tb_en_group -radix hex
save_wave_config ports_to_axis.wcfg
run all
quit

