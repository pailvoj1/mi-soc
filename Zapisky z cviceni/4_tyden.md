## Zed Board
- na webu zedboard.com - ![QuickStart guide](http://zedboard.com/sites/default/files/documentations/GS-AES-Z7EV-7Z020-G-V7-1.pdf)
- spuštění dema
- -  cd /usr/bin
- - read_sw - vrátí hodnotu nastavených switchů
- - write_led 0xff - roysvítí ledky nastavené na 1
- - Display demo - unload_oled, load_oled

- vypínaní Linuxu
- - před vypnutím pomocí SWITCHE uspat pomocí příkazu **poweroff**

- reference.digilentinc.com - Vivado ZedBoard tutorial, HDL Wrapper jsme nechali na Vivadu a bylo to **špatně**, Error 0xc...142 -> odinstalovat WinAVR