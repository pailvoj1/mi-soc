# MI-SOC

## Téma: Spolehlivý a rychlý přenos dat mezi CPU a FPGA na vývojové desce Zynq

## Úkoly a časový plán:

  * Harmonogram a přidělení úkolů naleznete zde - [Úkoly](https://coggle.it/diagram/XZ2eh9s7yM57mO6n/t/-/62a27ec3591bba68bea45737564958a245a7724ea4c3c6fb92933cb03c12b9b5)

  
## Prezentace
  * [Prezentace](prezentace/mi_soc_prez.pdf)
  
## Testovací architektura
![Testovací architektura](img/schema.png?raw=true)

  * Testovací architektura demonstruje přenos dat pomocí DMA mezi PL a PS.
  * Po stisknutí tlačítka (BUTTON) dojde k zakódování dat ze SWITCHů pomocí Hammingova kódu a k zachycení do registru v komponentě PORT\_TO\_AXIS
  * Odsud jsou data pomocí AXI Stream sběrnice přenesena do DMA a zapsána do paměti.
  * PS vyzvedne data z paměti, dekóduje je, invertuje bity, znovu zakóduje Hammingovým kódem a odešle do paměti
  * DMA vyzvedne data z paměti, sběrnicí AXI Stream je přenese do komponenty AXIS\_TO\_PORT, kde jsou data zachycena v registru
  * Data jsou dále dekódována a přivedena na LED diody

# Návod

## Spuštění na desce Zedboard
  * Stáhnout repozitář (git clone https://gitlab.fit.cvut.cz/pailvoj1/mi-soc.git)
  * Spustit Vivado SDK, na dotaz, kde vytvořit workspace zvolit složku sdk ve staženém repozitáři (mi-soc/src/sdk)
  
![Vytvoření Workspace](img/sdk_workspace.PNG?raw=true)
  * Po spuštění použijte File > Import...
  * Zvolte General > Existing Projects into Workspace a klepněte na Next
  
![Importování projektu](img/sdk_import.PNG?raw=true)
  * U Select root directory opět zvolte adresář SDK v repozitáři.
  * V Projects se zobrazí a vyberou 3 projekty, klepněte na Finish
  
![Importování projektu](img/sdk_import_final.PNG?raw=true)
  * Zavřete uvítací obrazovku, zobrazí se projekt.
  * Připojte Zedboard k PC pomocí 2 USB kabelů, jeden připojte do konektoru PROG, druhý do konektoru UART, připojte napájecí kabel a zapněte desku.
  * V SDK zvolte Xilinx > Program FPGA
  * V dialogovém okně stiskněte Program
  
![Programování FPGA](img/sdk_fpga_program.PNG?raw=true)
  * Pokud chcete sledovat debug výpis, připojte se libovolným terminálovým programem k portu Zedboardu, Baud Rate 115200.
  * V levé části SDK v Project Exploreru klepněte pravým tlačítkem na test_dma > Run As > Launch on Hardware (System Debugger)
  
![Programování CPU](img/sdk_program.PNG?raw=true)
  * Po nahrání programu můžete začít nastavovat SW7 - SW0, po stisknutí BTNC se zahají DMA přenos, data ze SW se invertují a odešlou zpět na LED7 - LED0.
  
## Otevření designu ve Vivadu
  * Stáhnout repozitář (git clone https://gitlab.fit.cvut.cz/pailvoj1/mi-soc.git)
  * Vytvořte si adresář, kde chcete vytvořit projekt, z repozitáře do něj nakopírujte adresář mi-soc/src/ip a soubor mi-soc/src/tcl/test_dma.tcl.
  * Spusťte Vivado
  * V dolní části uvítací obrazovky se nachází TCL Console, zadejte do ní příkaz cd s cestou k vašemu vytvořenému adresáři pro projekt.

![TCL CD](img/viv_cd.PNG?raw=true)
  * Zadejte do konzole source test_dma.tcl, dojde k vytvoření a otevření projektu.
  * V okně Sources klikněte na symbol + pro přidání souborů, zvolte Add or create constraints, klepněte na  Next. V dalším okně zvolte Add files a přidejte soubor const.xdc z repozitáře (mi-soc/src/constraints/const.xdc)
 
![Programování CPU](img/viv_source.PNG?raw=true)
  * Pokud chcete vygenerovat bitstream, je třeba provést následující:
    - Pravým tlačítkem klepněte na block design v okně Sources, zvolte Create HDL Wrapper, v dialogovém okně klepěte na OK
    
![Create HDL wrapper](img/viv_design.PNG?raw=true)

    - Následně již stačí jen spustit Generate Bitstream

  
  * Otevření block designu:
    - V levé části Vivada (Flow Navigator) zvolte Open Block Design

  * Exportování změněného HW do SDK:
    - Po vygenerování bitstreamu použijte File > Export > Export Hardware
    - V dialogovém okně zaškrtněte možnost Include bitstream a do cesty k exportu zadejte cestu k workspace SDK.
    
![Export HW](img/viv_exp.PNG?raw=true)

    - Poté použijte File > Launch SDK a do obou polí pro cesty zadejte cestu k workspace SDK.
    
![Launch SDK](img/viv_sdk.PNG?raw=true)
  
  
## Dokumentace
  * [Zynq technical reference manual](https://www.xilinx.com/support/documentation/user_guides/ug585-Zynq-7000-TRM.pdf)
  * [HLS](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2014_1/ug902-vivado-high-level-synthesis.pdf)
  * [AXI DMA Product guide](https://www.xilinx.com/support/documentation/ip_documentation/axi_dma/v7_1/pg021_axi_dma.pdf)

## Odkazy
  * http://www.fpgadeveloper.com/2014/08/using-the-axi-dma-in-vivado.html
  * https://github.com/fpgadeveloper/zedboard-axi-dma 
  * [AXI DMA](https://www.xilinx.com/support/documentation/ip_documentation/axi_dma/v7_1/pg021_axi_dma.pdf)
  * https://lauri.xn--vsandi-pxa.com/hdl/zynq/xilinx-dma.html
  * [Zynq video tutorial](https://www.youtube.com/watch?v=9FrgIfE2xfU)